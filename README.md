# Captive Portals Unlocker
This python script automatically unlocks annoying captive portals of public wifi networks. Currently it supports the captive portals of the wifi on ICE trains (_WIFIonICE_) and of hotsplots wifi networks. These are quite common in Germany, for example the wifi on the Stuttgart S-Bahn (_WIFI@DB_). I might add support for more networks after they annoyed me enough.

It can either be executed directly or launched by NetworkManager-dispatcher.

## Usage (directly)
```
Usage: captive_portals.py
Tries to unlock the captive portal.
```

## Usage (with nm-dispatcher)
Place it under `/etc/NetworkManager/dispatcher.d` and make sure that nm-dispatcher is enabled. When NetworkManager detects a captive portal, the script will automatically try to unlock it. Alternatively, you can append `_captive` to the connection name.
