#!/usr/bin/env python3
# Copyright (c) 2021 Lukas Fink
# Released under the MIT license. Read 'LICENSE' for more information.

import sys
import os
import pwd
import grp
import requests
from urllib.parse import urlparse, parse_qs
from html.parser import HTMLParser

TIMEOUT = 90
CAPTURE_URL = 'http://wikipedia.org/'
NM_DROP_PRIV_USER = 'nobody'
NM_DROP_PRIV_GROUP = 'nobody'

unlockers = {}
def unlocker(netloc):
    def add_unlocker(f):
        unlockers[netloc] = f
    return add_unlocker

class Error(Exception):
    pass

class URLError(Error):
    def __init__(self, url, message=None):
        if message == None:
            super().__init__('Unexpected url "' + url + '".')
        else:
            super().__init__(message)

        self.url = url

def parse_html_redirect(*args):
    class StopRedirectParsing(Exception):
        def __init__(self, url):
            self.url = url

    class HTMLRedirectParser(HTMLParser):
        def handle_starttag(self, tag, attrs):
            if tag != 'meta':
                return

            a = dict(attrs)
            if a.get('http-equiv', '').lower() != 'refresh':
                return

            s = a.get('content', '').split(';', 1)
            if len(s) != 2:
                return

            s = s[1].lstrip().split('=', 1)
            if len(s) != 2 or s[0].lower() != 'url':
                return

            raise StopRedirectParsing(s[1])

    p = HTMLRedirectParser()
    try:
        p.feed(*args)
    except StopRedirectParsing as e:
        return e.url

def parse_html_input(name, *args):
    class StopInputParsing(Exception):
        def __init__(self, value):
            self.value = value

    class HTMLInputParser(HTMLParser):
        def handle_starttag(self, tag, attrs):
            if tag != 'input':
                return

            a = dict(attrs)
            if 'name' not in a or a['name'] != name:
                return

            if 'value' not in a:
                return

            raise StopInputParsing(a['value'])

    p = HTMLInputParser()
    try:
        p.feed(*args)
    except StopInputParsing as e:
        return e.value

@unlocker('www.hotsplots.de')
def hotsplots(s, r):
    parsed_url = urlparse(r.url)
    q = parse_qs(parsed_url.query)
    q.update({'haveTerms': '1', 'termsOK': '', 'myLogin': 'agb',
              'button': 'kostenlos+einloggen', 'custom': '1', 'll': 'de'})
    r = s.post(parsed_url._replace(query='').geturl(), data=q, timeout=TIMEOUT)

    s.get(parse_html_redirect(r.text), timeout=TIMEOUT)

@unlocker('login.wifionice.de')
def wifionice(s, r):
    s.post(r.url, data={'login': 'true', 'CSRFToken': parse_html_input('CSRFToken', r.text)}, timeout=TIMEOUT)

def unlock():
    with requests.Session() as s:
        r = s.get(CAPTURE_URL, timeout=TIMEOUT)
        netloc = urlparse(r.url).netloc
        if netloc not in unlockers:
            raise URLError(r.url)
        unlockers[netloc](s, r)

def nm_dispatcher():
    if os.fork() != 0:
        return
    os.setgroups(())
    os.setgid(grp.getgrnam(NM_DROP_PRIV_GROUP).gr_gid)
    os.setuid(pwd.getpwnam(NM_DROP_PRIV_USER).pw_uid)

    action = os.environ['NM_DISPATCHER_ACTION']
    if ((action == 'connectivity-change' and os.environ.get('CONNECTIVITY_STATE') == 'PORTAL') or
        (action == 'up' and os.environ['CONNECTION_ID'].endswith('_captive'))):
        return unlock()

def main():
    if len(sys.argv) != 1:
        print('Usage: {}\n'
              'Tries to unlock the captive portal.'.format(sys.argv[0]))
        return
    return unlock()

if __name__ == '__main__':
    if 'NM_DISPATCHER_ACTION' in os.environ:
        nm_dispatcher()
    else:
        main()
